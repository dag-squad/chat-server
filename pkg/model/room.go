package model

type IceCandidate string

type Room struct {
	Offer  Sdp
	Answer Sdp

	OfferCandidates  []IceCandidate
	AnswerCandidates []IceCandidate
}
