package model

import "github.com/teris-io/shortid"

type Sdp string

type RoomID string

func NewRoomID() RoomID {
	return RoomID(shortid.MustGenerate())
}

func NewShortRoomID(length int) RoomID {
	idStr := shortid.MustGenerate()
	if length <= 0 || length > len(idStr) {
		length = len(idStr)
	}
	return RoomID(idStr[:length])
}
