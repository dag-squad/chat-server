package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"

	"gitlab.com/dag-squad/chat-server/pkg/model"
	"gitlab.com/dag-squad/chat-server/pkg/store"
)

func CreateRoom(store store.Rooms) func(c *gin.Context) {
	return func(c *gin.Context) {
		roomID := store.CreateRoom()

		log.WithField("room_id", roomID).Info("created room")

		c.JSON(http.StatusOK, gin.H{
			"roomId": roomID,
		})
	}
}

func AddOffer(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	type bindData struct {
		Sdp  model.Sdp `json:"sdp"`
		Type string    `json:"type"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		var bind bindData
		err = c.ShouldBindJSON(&bind)
		if err != nil {
			log.WithError(err).Error("can't parse json data")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse json body",
			})
			return
		}
		bytes, err := json.Marshal(bind)
		if err != nil {
			log.WithError(err).Error("can't unmarshal")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "can't unmarshal",
			})
			return
		}
		sdp := model.Sdp(bytes)

		log.WithFields(log.Fields{
			"sdp":    bind.Sdp,
			"type":   bind.Type,
			"roomID": uriBind.RoomID,
		}).Info("Added offer")

		err = store.AddOffer(uriBind.RoomID, sdp)
		if err != nil {
			log.WithError(err).Error("can't add offer")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't add offer",
			})
			return
		}

		c.Status(http.StatusOK)
	}
}

func GetOffer(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		offer, err := store.GetOffer(uriBind.RoomID)
		if err != nil {
			c.JSON(http.StatusAccepted, gin.H{
				"status": "room not found",
			})
			return
		}

		if offer == "" {
			c.JSON(http.StatusAccepted, gin.H{
				"status": fmt.Sprintf("offer not found for room %s", uriBind.RoomID),
			})
			return
		}

		var rawMessage json.RawMessage
		err = json.Unmarshal([]byte(offer), &rawMessage)
		if err != nil {
			log.WithError(err).Error("can't create json response")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't create json response",
			})
			return
		}

		log.WithFields(log.Fields{
			"room_id": uriBind.RoomID,
			"offer":   offer,
		}).Info("get offer request")

		c.JSON(http.StatusOK, rawMessage)
	}
}

func AddAnswer(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	type bindData struct {
		Sdp  model.Sdp `json:"sdp"`
		Type string    `json:"type"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		var bind bindData
		err = c.ShouldBindJSON(&bind)
		if err != nil {
			log.WithError(err).Error("can't parse json data")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse json body",
			})
			return
		}
		bytes, err := json.Marshal(bind)
		if err != nil {
			log.WithError(err).Error("can't unmarshal")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "can't unmarshal",
			})
			return
		}
		sdp := model.Sdp(bytes)

		err = store.AddAnswer(uriBind.RoomID, sdp)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{
				"status": "can't add answer",
			})
			return
		}

		log.WithFields(log.Fields{
			"answer":  bind.Sdp,
			"type":    bind.Type,
			"room_id": uriBind.RoomID,
		}).Info()

		c.Status(http.StatusOK)
	}
}

func GetAnswerForRoom(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		answer, err := store.GetAnswer(uriBind.RoomID)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{
				"status": "room not found",
			})
			return
		}
		if answer == "" {
			c.JSON(http.StatusAccepted, gin.H{
				"status": fmt.Sprintf("answer not found for room %s", uriBind.RoomID),
			})
			return
		}

		var rawMessage json.RawMessage
		err = json.Unmarshal([]byte(answer), &rawMessage)
		if err != nil {
			log.WithError(err).Error("can't create json response")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't create json response",
			})
			return
		}

		log.WithFields(log.Fields{
			"answer":  answer,
			"room_id": uriBind.RoomID,
		}).Info()

		c.JSON(http.StatusOK, rawMessage)
	}
}

func AddOfferCandidates(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}

	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		var rawMessage json.RawMessage
		err = c.ShouldBindJSON(&rawMessage)
		if err != nil {
			log.WithError(err).Error("can't read body")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to read body",
			})
			return
		}
		bytes, err := json.Marshal(rawMessage)
		if err != nil {
			log.WithError(err).Error("can't unmarshal")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "can't unmarshal",
			})
			return
		}
		candidate := model.IceCandidate(bytes)

		err = store.AddOfferCandidates(uriBind.RoomID, []model.IceCandidate{candidate})
		if err != nil {
			log.WithError(err).Error("can't store offer candidate")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't store offer candidate",
			})
			return
		}

		c.Status(http.StatusOK)
	}
}

func GetOfferCandidates(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		candidatesStrings, err := store.GetOfferCandidates(uriBind.RoomID)
		if err != nil {
			log.WithError(err).Error("can't get offer candidates")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't get offer candidates",
			})
			return
		}

		var candidates []json.RawMessage
		for _, str := range candidatesStrings {
			var rawMessage json.RawMessage
			err = json.Unmarshal([]byte(str), &rawMessage)
			if err != nil {
				log.WithError(err).Error("can't create json response")
				c.JSON(http.StatusInternalServerError, gin.H{
					"status": "can't create json response",
				})
				return
			}
			candidates = append(candidates, rawMessage)
		}

		c.JSON(http.StatusOK, gin.H{
			"candidates": candidates,
		})
	}
}

func AddAnswerCandidates(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		var rawMessage json.RawMessage
		err = c.ShouldBindJSON(&rawMessage)
		if err != nil {
			log.WithError(err).Error("can't read body")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to read body",
			})
			return
		}
		bytes, err := json.Marshal(rawMessage)
		if err != nil {
			log.WithError(err).Error("can't unmarshal")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "can't unmarshal",
			})
			return
		}
		candidate := model.IceCandidate(bytes)

		err = store.AddAnswerCandidates(uriBind.RoomID, []model.IceCandidate{candidate})
		if err != nil {
			log.WithError(err).Error("can't store answer candidate")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't store answer candidate",
			})
			return
		}

		c.Status(http.StatusOK)
	}
}

func GetAnswerCandidates(store store.Rooms) func(c *gin.Context) {
	type uriBindData struct {
		RoomID model.RoomID `uri:"room_id" bindings:"required"`
	}
	return func(c *gin.Context) {
		var uriBind uriBindData
		err := c.ShouldBindUri(&uriBind)
		if err != nil {
			log.WithError(err).Error("can't bind uri")
			c.JSON(http.StatusBadRequest, gin.H{
				"status": "unable to parse uri parameters",
			})
			return
		}

		candidatesStrings, err := store.GetAnswerCandidates(uriBind.RoomID)
		if err != nil {
			log.WithError(err).Error("can't get answer candidates")
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": "can't get answer candidates",
			})
			return
		}

		var candidates []json.RawMessage
		for _, str := range candidatesStrings {
			var rawMessage json.RawMessage
			err = json.Unmarshal([]byte(str), &rawMessage)
			if err != nil {
				log.WithError(err).Error("can't create json response")
				c.JSON(http.StatusInternalServerError, gin.H{
					"status": "can't create json response",
				})
				return
			}
			candidates = append(candidates, rawMessage)
		}

		c.JSON(http.StatusOK, gin.H{
			"candidates": candidates,
		})
	}
}
