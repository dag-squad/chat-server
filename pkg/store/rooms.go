package store

import (
	"errors"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/dag-squad/chat-server/pkg/model"
)

type Rooms interface {
	CreateRoom() model.RoomID
	GetRoom(id model.RoomID) (model.Room, bool) // Testing only

	AddOffer(id model.RoomID, offer model.Sdp) error
	GetOffer(id model.RoomID) (model.Sdp, error)

	AddAnswer(id model.RoomID, answer model.Sdp) error
	GetAnswer(id model.RoomID) (model.Sdp, error)

	AddOfferCandidates(id model.RoomID, offerCandidates []model.IceCandidate) error
	GetOfferCandidates(id model.RoomID) ([]model.IceCandidate, error)

	AddAnswerCandidates(id model.RoomID, answerCandidates []model.IceCandidate) error
	GetAnswerCandidates(id model.RoomID) ([]model.IceCandidate, error)
}

type InMemoryRooms struct {
	rooms map[model.RoomID]*model.Room
	sync.RWMutex
}

func NewInMemoryRooms() *InMemoryRooms {
	return &InMemoryRooms{rooms: make(map[model.RoomID]*model.Room)}
}

func (r *InMemoryRooms) CreateRoom() model.RoomID {
	r.Lock()
	defer r.Unlock()

	roomID := model.NewShortRoomID(3)
	for {
		if _, ok := r.rooms[roomID]; !ok {
			break
		}
		roomID = model.NewShortRoomID(3)
	}
	r.rooms[roomID] = &model.Room{}
	return roomID
}

func (r *InMemoryRooms) GetRoom(id model.RoomID) (model.Room, bool) {
	r.RLock()
	defer r.Unlock()

	room, ok := r.rooms[id]
	if !ok {
		return model.Room{}, false
	}
	return *room, true
}

func (r *InMemoryRooms) AddOffer(id model.RoomID, offer model.Sdp) error {
	r.Lock()
	defer r.Unlock()

	room, ok := r.rooms[id]
	if !ok {
		return errors.New("room not found")
	}

	if room.Offer != "" {
		log.WithFields(log.Fields{
			"room_id":   id,
			"old_offer": room.Offer,
			"new_offer": offer,
		}).Warn("offer is not empty")
	}
	room.Offer = offer
	return nil
}

func (r *InMemoryRooms) GetOffer(id model.RoomID) (model.Sdp, error) {
	r.RLock()
	defer r.RUnlock()

	room, ok := r.rooms[id]
	if !ok {
		return "", errors.New("room not found")
	}

	return room.Offer, nil
}

func (r *InMemoryRooms) AddAnswer(id model.RoomID, answer model.Sdp) error {
	r.Lock()
	defer r.Unlock()

	room, ok := r.rooms[id]
	if !ok {
		return errors.New("room not found")
	}

	if room.Answer != "" {
		log.WithFields(log.Fields{
			"room_id":    id,
			"old_answer": room.Answer,
			"new_answer": answer,
		}).Warn("answer is not empty")
	}

	room.Answer = answer
	return nil
}

func (r *InMemoryRooms) GetAnswer(id model.RoomID) (model.Sdp, error) {
	r.RLock()
	defer r.RUnlock()

	room, ok := r.rooms[id]
	if !ok {
		return "", errors.New("room not found")
	}

	return room.Answer, nil
}

func (r *InMemoryRooms) AddOfferCandidates(id model.RoomID, offerCandidates []model.IceCandidate) error {
	r.Lock()
	defer r.Unlock()

	room, ok := r.rooms[id]
	if !ok {
		return errors.New("room not found")
	}

	room.OfferCandidates = append(room.OfferCandidates, offerCandidates...)
	return nil
}

func (r *InMemoryRooms) GetOfferCandidates(id model.RoomID) ([]model.IceCandidate, error) {
	r.RLock()
	defer r.RUnlock()

	room, ok := r.rooms[id]
	if !ok {
		return nil, errors.New("room not found")
	}

	offerCandidates := make([]model.IceCandidate, len(room.OfferCandidates))
	copy(offerCandidates, room.OfferCandidates)
	return offerCandidates, nil
}

func (r *InMemoryRooms) AddAnswerCandidates(id model.RoomID, answerCandidates []model.IceCandidate) error {
	r.Lock()
	defer r.Unlock()

	room, ok := r.rooms[id]
	if !ok {
		return errors.New("room not found")
	}

	room.AnswerCandidates = append(room.AnswerCandidates, answerCandidates...)
	return nil
}

func (r *InMemoryRooms) GetAnswerCandidates(id model.RoomID) ([]model.IceCandidate, error) {
	r.RLock()
	defer r.RUnlock()

	room, ok := r.rooms[id]
	if !ok {
		return nil, errors.New("room not found")
	}

	answerCandidates := make([]model.IceCandidate, len(room.AnswerCandidates))
	copy(answerCandidates, room.AnswerCandidates)
	return answerCandidates, nil
}
