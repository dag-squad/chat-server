FROM golang:latest
# Using golang modules
ENV GO111MODULE=on

# Create dir for application
WORKDIR /app

# Download dependencies
COPY go.mod ./
RUN go mod download

# Copy all project files to application dir
COPY . ./

# Build binary from `./cmd/chat-server` main package. Output binary file to project root and name it `chat-server`
RUN go build -o chat-server -v ./cmd/chat-server

# Run `./chat-server` command on container start
CMD ./chat-server
