module gitlab.com/dag-squad/chat-server

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/pion/rtcp v1.2.6
	github.com/pion/webrtc/v3 v3.0.31
	github.com/sirupsen/logrus v1.8.1
	github.com/teris-io/shortid v0.0.0-20201117134242-e59966efd125
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
)
