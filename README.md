<h1>💣Golang chat server💣</h1>

<h3>Default ports</h3>

HTTPS server https://localhost:44301

HTTP redirect: http://localhost:8080/<PATH> => https://localhost:44301/<PATH>

<h3>Endpoints</h3>

```
POST   /rooms                            
POST   /rooms/:room_id/offer     
GET    /rooms/:room_id/offer     
POST   /rooms/:room_id/answer    
GET    /rooms/:room_id/answer    
POST   /rooms/:room_id/offer-candidates 
GET    /rooms/:room_id/offer-candidates 
POST   /rooms/:room_id/answer-candidates 
GET    /rooms/:room_id/answer-candidates
```

<h3>Run from CMD</h3>
install golang https://golang.org/

install dependencies
```
go mod download
```

cd to project root and:
```
go run ./cmd/chat-server
```

<h3>Run from docker</h3>

```shell
docker run -it --rm  \
  --name chat-server \
  -p 8080:8080       \
  -p 44301:44301     \
  dvkim/dag-squad-chat-server:v0.0.1
```

Change HOST ports if needed `-p <HOST_PORT>:<CONTAINER_PORT>`. Keep container ports.

Example:
```
deeonisio@ubuntu-desktop:~/dag-squad/chat-server$ docker run -it --rm --name chat-server -p 8080:8080 -p 44301:44301 dvkim/dag-squad-chat-server:v0.0.1
Unable to find image 'dvkim/dag-squad-chat-server:v0.0.1' locally
v0.0.1: Pulling from dvkim/dag-squad-chat-server
d960726af2be: Already exists 
e8d62473a22d: Already exists 
8962bc0fad55: Already exists 
65d943ee54c1: Already exists 
f2253e6fbefa: Already exists 
6d7fa7c7d5d3: Already exists 
e2e442f7f89f: Already exists 
d3b3318c140e: Pull complete 
950973f0b1ac: Pull complete 
88359fbf7ab8: Pull complete 
93759f2a19eb: Pull complete 
0e66cde8c740: Pull complete 
Digest: sha256:4e1f4541f6f48749cb3cdd2c4d667673a8b6e8552539850e53d251662e7215a2
Status: Downloaded newer image for dvkim/dag-squad-chat-server:v0.0.1
[GIN-debug] [WARNING] Creating an Engine instance with the Logger and Recovery middleware already attached.

[GIN-debug] [WARNING] Running in "debug" mode. Switch to "release" mode in production.
 - using env:	export GIN_MODE=release
 - using code:	gin.SetMode(gin.ReleaseMode)

[GIN-debug] POST   /rooms                    --> gitlab.com/dag-squad/chat-server/pkg/handlers.CreateRoom.func1 (4 handlers)
[GIN-debug] POST   /rooms/:room_id/offer     --> gitlab.com/dag-squad/chat-server/pkg/handlers.AddOffer.func1 (4 handlers)
[GIN-debug] GET    /rooms/:room_id/offer     --> gitlab.com/dag-squad/chat-server/pkg/handlers.GetOffer.func1 (4 handlers)
[GIN-debug] POST   /rooms/:room_id/answer    --> gitlab.com/dag-squad/chat-server/pkg/handlers.AddAnswer.func1 (4 handlers)
[GIN-debug] GET    /rooms/:room_id/answer    --> gitlab.com/dag-squad/chat-server/pkg/handlers.GetAnswerForRoom.func1 (4 handlers)
[GIN-debug] POST   /rooms/:room_id/offer-candidates --> gitlab.com/dag-squad/chat-server/pkg/handlers.AddOfferCandidates.func1 (4 handlers)
[GIN-debug] GET    /rooms/:room_id/offer-candidates --> gitlab.com/dag-squad/chat-server/pkg/handlers.GetOfferCandidates.func1 (4 handlers)
[GIN-debug] POST   /rooms/:room_id/answer-candidates --> gitlab.com/dag-squad/chat-server/pkg/handlers.AddAnswerCandidates.func1 (4 handlers)
[GIN-debug] GET    /rooms/:room_id/answer-candidates --> gitlab.com/dag-squad/chat-server/pkg/handlers.GetAnswerCandidates.func1 (4 handlers)
INFO[0000] Starting Localhost TLS server                
INFO[0000] Creating self-signed certificates             certPath=./certs/localhost.crt keyPath=./certs/localhost.key
INFO[0000] starting localhost HTTPS server               port=44301
INFO[0000] starting HTTP server for redirecting to HTTPS  port=8080
^CINFO[0019] Shutdown server ...                          
WARN[0019] chat server stopped                           error="http: Server closed"
INFO[0019] Server exited                                
deeonisio@ubuntu-desktop:~/dag-squad/chat-server$ 
```

