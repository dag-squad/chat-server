package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/acme/autocert"
)

type runTLSOptions struct {
	HttpsPort   int
	LetsEncrypt bool
	Domain      string
}

func runTLS(r *gin.Engine, options runTLSOptions) *http.Server {
	if options.LetsEncrypt {
		log.Info("Starting Lets Encrypt TLS server")
		if options.Domain == "" {
			log.WithField("value", "")
			options.Domain = "kimdv.club"
		}
	} else {
		log.Info("Starting Localhost TLS server")
	}
	if options.HttpsPort == 0 {
		log.WithField("value", 44301).Info("Using default HTTPS port")
		options.HttpsPort = 44301
	}

	httpsPort := fmt.Sprintf(":%d", options.HttpsPort)
	if !options.LetsEncrypt {
		certPath, keyPath := "dev-certs/192.168.88.2+254.pem", "dev-certs/192.168.88.2+254-key.pem"

		s := &http.Server{
			Addr:           httpsPort,
			Handler:        r,
			ReadTimeout:    30 * time.Second,
			WriteTimeout:   30 * time.Second,
			MaxHeaderBytes: 8 << 20,
		}

		go func() {
			log.WithField("port", options.HttpsPort).Info("starting localhost HTTPS server")
			if err := s.ListenAndServeTLS(certPath, keyPath); err != nil {
				log.WithError(err).Warn("chat server stopped")
			}
		}()

		return s
	}
	m := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		HostPolicy: autocert.HostWhitelist(options.Domain),
		Cache:      autocert.DirCache("./dev-certs"),
		Email:      "profick123@gmail.com",
	}

	s := &http.Server{
		Addr:           httpsPort,
		TLSConfig:      m.TLSConfig(),
		Handler:        r,
		ReadTimeout:    30 * time.Second,
		WriteTimeout:   30 * time.Second,
		MaxHeaderBytes: 8 << 20,
	}

	go func() {
		log.WithField("port", options.HttpsPort).Info("starting Lets Encrypt HTTPS server")
		if err := s.ListenAndServeTLS("", ""); err != nil {
			log.WithError(err).Warn("chat server stopped")
		}
	}()
	return s
}
