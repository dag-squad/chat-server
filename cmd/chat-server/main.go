package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"github.com/teris-io/shortid"

	"gitlab.com/dag-squad/chat-server/pkg/handlers"
	"gitlab.com/dag-squad/chat-server/pkg/store"
)

func main() {
	sid, err := shortid.New(1, shortid.DefaultABC, 1234)
	if err != nil {
		log.WithError(err).Fatal("can't setup shortid")
	}
	shortid.SetDefault(sid)

	var roomsStore store.Rooms = store.NewInMemoryRooms()

	r := gin.Default()
	r.Use(cors.Default())

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "ok!"})
	})
	r.POST("/rooms", handlers.CreateRoom(roomsStore))
	roomsGroup := r.Group("/rooms")
	roomsGroup.POST("/:room_id/offer", handlers.AddOffer(roomsStore))
	roomsGroup.GET("/:room_id/offer", handlers.GetOffer(roomsStore))
	roomsGroup.POST("/:room_id/answer", handlers.AddAnswer(roomsStore))
	roomsGroup.GET("/:room_id/answer", handlers.GetAnswerForRoom(roomsStore))

	roomsGroup.POST("/:room_id/offer-candidates", handlers.AddOfferCandidates(roomsStore))
	roomsGroup.GET("/:room_id/offer-candidates", handlers.GetOfferCandidates(roomsStore))
	roomsGroup.POST("/:room_id/answer-candidates", handlers.AddAnswerCandidates(roomsStore))
	roomsGroup.GET("/:room_id/answer-candidates", handlers.GetAnswerCandidates(roomsStore))

	srv := runTLS(r, runTLSOptions{
		HttpsPort:   44301,
		LetsEncrypt: false,
		Domain:      "",
	})

	// Graceful shutdown
	quit := make(chan os.Signal, 2)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Info("Shutdown server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.WithError(err).Fatal("Server forced to shutdown")
	}
	log.Info("Server exited")
}
